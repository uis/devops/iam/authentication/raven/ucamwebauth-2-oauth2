# Use python alpine image to run webapp proper
FROM registry.gitlab.developers.cam.ac.uk/uis/devops/infra/dockerimages/django:3.2-py3.9-alpine

# Ensure packages are up to date and install some useful utilities
RUN apk update && apk add git vim postgresql-dev libffi-dev gcc musl-dev \
	libxml2-dev libxslt-dev

# From now on, work in the application directory
WORKDIR /usr/src/app

# Copy Docker configuration and install any requirements. We install
# requirements/docker.txt last to allow it to override any versions in
# requirements/requirements.txt.
ADD ./requirements/* ./requirements/
RUN \
	apk add --no-cache build-base \
		postgresql-dev libffi-dev libxml2-dev libxslt-dev \
	&& pip install --no-cache-dir -r requirements/base.txt \
	&& pip install --no-cache-dir -r requirements/docker.txt \
	&& apk del g++ gcc binutils

# Copy the remaining files over
ADD . .

# Default environment for image.  By default, we use the settings module bundled
# with this repo. Change DJANGO_SETTINGS_MODULE to install a custom settings.
#
# You probably want to modify the following environment variables:
#
# DJANGO_DB_ENGINE, DJANGO_DB_HOST, DJANGO_DB_PORT, DJANGO_DB_USER
EXPOSE 8000
ENV \
	DJANGO_SETTINGS_MODULE=project.settings.docker \
	PORT=8000

# Collect static files. We provide placeholder values for required settings.
RUN \
    EXTERNAL_SETTING_SECRET_KEY=placeholder \
    EXTERNAL_SETTING_DATABASES="{'default': {}}" \
    EXTERNAL_SETTING_SOCIAL_AUTH_GOOGLE_OAUTH2_KEY=placeholder \
    EXTERNAL_SETTING_SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET=placeholder \
    EXTERNAL_SETTING_WLS2OAUTH2_PRIVATE_KEY=placeholder \
    EXTERNAL_SETTING_WLS2OAUTH2_KEY_ID=placeholder \
    ./manage.py collectstatic

ENTRYPOINT ["./docker-entrypoint.sh"]
