# Import settings from the base settings file
from .base import *  # noqa: F401, F403

# Logging
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': ('%(levelname)s %(asctime)s %(pathname)s:%(lineno)d '
                       '%(funcName)s "%(message)s"')
        },
        'simple': {
            'format': '%(levelname)s "%(message)s"'
        },
    },
    'handlers': {
        'console': {
            'level': 'INFO',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose',
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['console', 'mail_admins'],
            'propagate': True,
            'level': 'INFO'
        },
    }
}
