UCam WebAuth to OAuth2 Shim
===============================================================================

Installation
````````````

Add the ``wls2oauth2`` application to your
``INSTALLED_APPS`` configuration as usual.

Views and serializers
`````````````````````

.. automodule:: wls2oauth2.views
    :members:

Default URL routing
```````````````````

.. automodule:: wls2oauth2.urls
    :members:

Application configuration
`````````````````````````

.. automodule:: wls2oauth2.apps
    :members:
