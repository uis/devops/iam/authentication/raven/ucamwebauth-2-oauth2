import urllib.parse
from unittest import mock

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase, RequestFactory
from django.urls import reverse
import ucam_wls.status
from ucamwebauth import RavenResponse
from ucamwebauth.backends import RavenAuthBackend

from .. import views


class AuthenticateViewTestCase(TestCase):
    def setUp(self):
        self.view = views.AuthenticateView.as_view()
        self.user = get_user_model().objects.create(username='test0001')

        # Where the auth view lives in the URL path.
        self.view_path = reverse('wls2oauth2_authenticate')

        # A fake full URL to the authentication view
        self.view_url = f'https://example.invalid/{self.view_path}'

        # A backend which can be used to authenticate the returned auth response
        self.auth_backend = RavenAuthBackend()

    def test_disallowed_methods(self):
        """
        Standard methods other than HEAD, OPTIONS and GET are not allowed.

        """
        for method in ['POST', 'PUT', 'PATCH', 'DELETE', 'TRACE']:
            with self.subTest(method=method):
                response = self.client.generic(method, self.view_path)
                self.assertEqual(response.status_code, 405)  # method not allowed

    def test_no_query_parameters(self):
        """
        An empty query returns a "General request parameter error" status.

        """
        response = self.get()
        self.assertEqual(response.status_code, ucam_wls.status.REQUEST_PARAM_ERROR)

    def test_unsupported_version(self):
        """
        An unsupported version string returns an "Unsupported protocol version" error.

        """
        response = self.get(self.auth_request_query_dict(ver=0))
        self.assertEqual(response.status_code, ucam_wls.status.UNSUPPORTED_PROTO_VER)
        response = self.get(self.auth_request_query_dict(ver=4))
        self.assertEqual(response.status_code, ucam_wls.status.UNSUPPORTED_PROTO_VER)

    def test_non_numeric_version(self):
        """
        A non-numeric version returns a "General request parameter error" status.

        """
        response = self.get(self.auth_request_query_dict(ver='hello'))
        self.assertEqual(response.status_code, ucam_wls.status.REQUEST_PARAM_ERROR)

    def test_unexpected_exception_parsing_query(self):
        """
        Any unexpected exception parsing the query string returns a "General request parameter
        error" status.

        """
        p = mock.patch('ucam_wls.AuthRequest.from_query_string')
        with p as mock_from_query_string:
            mock_from_query_string.side_effect = RuntimeError
            response = self.get(self.auth_request_query_dict())
            mock_from_query_string.assert_called_once()
        self.assertEqual(response.status_code, ucam_wls.status.REQUEST_PARAM_ERROR)

    def test_unsupported_auth_method(self):
        """
        An unsupported auth method returns a "No mutually acceptable authentication types
        available" status.

        """
        response = self.get(self.auth_request_query_dict(aauth='NOT_AN_AUTH_TYPE'))
        self.assertEqual(response.status_code, ucam_wls.status.NO_MUTUAL_AUTH_TYPES)

    def test_no_iact_and_existing_session(self):
        """
        If iact is 'no' and there is an existing session, authentication succeeds.

        """
        response = self.get(self.auth_request_query_dict(iact='no'), user=self.user)
        self.assert_response_is_valid(response)

    def test_no_iact_and_no_existing_session(self):
        """
        If iact is 'no' and there is no existing session, an "Interaction required" status is
        returned.

        """
        response = self.get(self.auth_request_query_dict(iact='no'))
        self.assertEqual(response.status_code, ucam_wls.status.INTERACTION_REQUIRED)

    def test_yes_iact_and_existing_session(self):
        """
        If iact is 'yes' and there is an existing session, authentication succeeds but only after
        redirecting to the login page.

        """
        response = self.expect_login_redirect(
            self.get(self.auth_request_query_dict(iact='yes'), user=self.user)
        )
        self.assert_response_is_valid(response)

    def test_yes_iact_and_no_existing_session(self):
        """
        If iact is 'yes' and there is no existing session, authentication succeeds but only after
        redirecting to the login page.

        """
        response = self.expect_login_redirect(self.get(self.auth_request_query_dict(iact='yes')))
        self.assert_response_is_valid(response)

    def test_existing_session(self):
        """
        Existing login session allows non-interactive login if iact has no value.

        """
        response = self.get(self.auth_request_query_dict(), user=self.user)
        self.assert_response_is_valid(response)

    def test_no_existing_session(self):
        """
        No existing login session requires interactive login is iact has no value.

        """
        response = self.expect_login_redirect(self.get(self.auth_request_query_dict()))
        self.assert_response_is_valid(response)

    def test_no_iact_and_no_existing_session_redirect_to_client(self):
        """
        If iact is 'no' and there is no existing session, and fail is not set, an "Interaction
        required" redirect is returned.

        """
        response = self.get(self.auth_request_query_dict(iact='no', fail=None))
        self.assert_response_raises_status(response, ucam_wls.status.INTERACTION_REQUIRED)

    def test_unsupported_auth_method_redirect_to_client(self):
        """
        An unsupported auth method returns a "No mutually acceptable authentication types
        available" redirect if fail is not "yes".

        """
        response = self.get(self.auth_request_query_dict(aauth='NOT_AN_AUTH_TYPE', fail=None))
        self.assert_response_raises_status(response, ucam_wls.status.NO_MUTUAL_AUTH_TYPES)

    def test_params_preserved(self):
        """
        Request query parameters are preserved via "params" field in response even through
        an interactive authentication.

        """
        params_dict = {'foo': ['bar/with&special?characters<>', 'a second foo query']}
        response = self.expect_login_redirect(self.get(self.auth_request_query_dict(
            iact='yes', params=urllib.parse.urlencode(params_dict, doseq=True)
        )))
        response = self.assert_response_is_valid(response)
        self.assertEqual(response.params, params_dict)

    def get_redirect_url_from_response(self, response, expected_url):
        """
        Extract the redirect URL from a redirect response from the WLS. Assert that the response
        *is* a redirect and that it corresponds to the expected URL.

        """
        # The response should be a redirect.
        self.assertEqual(response.status_code, 302)  # some redirect

        # Parse the redirect URL
        url = response.get('Location')
        url_parts = urllib.parse.urlsplit(url)

        # Check that the URL minus query parameters is as expected.
        redirect_base_url = urllib.parse.urlunsplit((
            url_parts.scheme, url_parts.netloc, url_parts.path, '', url_parts.fragment
        ))
        self.assertEqual(expected_url, redirect_base_url)

        # Return the redirect URL
        return url

    def assert_response_is_valid(self, response, userid=None, expected_url=None):
        """
        Assert that the authentication response corresponds to a given userid. If no userid is
        given, the username of self.user is used. Returns the parsed RavenResponse from
        django-ucamwebauth.

        """
        userid = userid if userid is not None else self.user.username
        expected_url = expected_url if expected_url is not None else self.view_url
        url = self.get_redirect_url_from_response(response, expected_url)

        # Attempt to authenticate. We trust django-ucamwebauth will validate the requests
        # appropriately.
        factory = RequestFactory()
        with self.settings(UCAMWEBAUTH_RETURN_URL=expected_url):
            user = self.auth_backend.authenticate(factory.get(url))

        # Assert that user is returned, is not anonymous and is the expected one.
        self.assertIsNotNone(user)
        self.assertFalse(user.is_anonymous)
        self.assertEqual(user.username, userid)

        # Return a parse RavenResponse
        with self.settings(UCAMWEBAUTH_RETURN_URL=expected_url):
            response = RavenResponse(factory.get(url))
        return response

    def assert_response_raises_status(self, response, expected_status, expected_url=None):
        """
        Assert that the authentication response corresponds to a failure and that the status
        matches the expected status. Returns the parsed RavenResponse from django-ucamwebauth.

        """
        expected_url = expected_url if expected_url is not None else self.view_url
        url = self.get_redirect_url_from_response(response, expected_url)

        # Use ucamwebauth's RavenResponse class to parse the response
        factory = RequestFactory()
        with self.settings(UCAMWEBAUTH_RETURN_URL=expected_url):
            response = RavenResponse(factory.get(url))

        # The response should not be successful.
        self.assertFalse(response.validate())

        # The response's status code should match the expected status.
        self.assertEqual(response.status, expected_status)

        return response

    def auth_request_query_dict(
            self, *, ver=3, url=None, desc=None, aauth=None, iact=None,
            msg=None, params=None, date=None, skew=None, fail='yes'):
        """
        Construct and return a query parameter dictionary representing an authentication request.
        The arguments correspond to the parameters listed in v4.1 of the waa2wls specification.

        """
        if url is None:
            url = self.view_url

        # Add required parameters
        query = {'ver': str(ver), 'url': url}
        if desc is not None:
            query['desc'] = desc
        if aauth is not None:
            query['aauth'] = aauth
        if iact is not None:
            query['iact'] = iact
        if msg is not None:
            query['msg'] = msg
        if params is not None:
            query['params'] = params
        if date is not None:
            query['date'] = date
        if skew is not None:
            query['skew'] = skew
        if fail is not None:
            query['fail'] = fail
        return query

    def get(self, query=None, *, user=None):
        """
        Return the response from a HTTP GET request passed to the authentication view. *query* is
        the query string or a dictionary of query parameters. If *user* is not None, it specifies
        the authenticated user. If *user* is None then the anonymous user is used.

        """
        if isinstance(query, str):
            qs = query
        else:
            qs = urllib.parse.urlencode(query if query is not None else {})
        if user is not None:
            self.client.force_login(user)
        return self.client.get(f'{self.view_path}?{qs}')

    def expect_login_redirect(self, response, *, user=None):
        """
        Expects and asserts a login redirect response back to the authentication view. The user,
        if specified, cannot be anonymous. If omitted, the value of self.user is used.

        If the redirect is valid, return the value of self.get() passing the appropriate query and
        user.

        """
        user = user if user is not None else self.user
        self.assertFalse(user.is_anonymous)

        # Response should redirect
        self.assertEqual(response.status_code, 302)  # some redirect

        # Parse the redirect URL
        url = response.get('Location')
        url_parts = urllib.parse.urlsplit(url)

        # Stripped of query, the URL should match the login URL
        redirect_base_url = urllib.parse.urlunsplit((
            url_parts.scheme, url_parts.netloc, url_parts.path, '', url_parts.fragment
        ))
        self.assertEqual(redirect_base_url, settings.LOGIN_URL)

        # Parse the redirect URL query
        redirect_query = urllib.parse.parse_qs(url_parts.query)

        # "next" should be a query parameter
        self.assertIn('next', redirect_query)
        self.assertEqual(len(redirect_query['next']), 1)
        next_url = redirect_query['next'][0]
        self.assertIsNotNone(next_url)

        # Parse the next URL and check that it points back to the authentication view
        next_url_parts = urllib.parse.urlsplit(next_url)
        next_base_url = urllib.parse.urlunsplit((
            next_url_parts.scheme, next_url_parts.netloc, next_url_parts.path, '',
            next_url_parts.fragment
        ))
        self.assertEqual(next_base_url, self.view_path)

        # Call the authentication view, passing the query and signing in the user
        return self.get(next_url_parts.query, user=user)
