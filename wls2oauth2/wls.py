from cryptography.hazmat.primitives.serialization import load_pem_private_key
from cryptography.hazmat.backends import default_backend
from django.conf import settings
from ucam_wls import LoginService
from ucam_wls.signing import Key


def get_login_service():
    """
    Return a ucam_wls.LoginService configured for this application.

    """
    # Currently "pwd" is the only supported auth method.
    return LoginService(key=get_private_key(), auth_methods=['pwd'])


def get_private_key():
    """
    Return a ucam_wls.signing.Key instance corresponding to the application's private key.

    """
    return Key(
        load_pem_private_key(
            settings.WLS2OAUTH2_PRIVATE_KEY.encode('ascii'), None, default_backend()
        ),
        settings.WLS2OAUTH2_KEY_ID
    )
