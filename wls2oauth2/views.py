import datetime
import logging
import urllib.parse

from django.conf import settings
from django.shortcuts import redirect
from django.template.response import SimpleTemplateResponse
from django.views import View
import ucam_wls
from ucam_wls.errors import InvalidAuthRequest, ProtocolVersionUnsupported
import ucam_wls.status

from .wls import get_login_service

LOG = logging.getLogger(__name__)


class ErrorResponseBase(SimpleTemplateResponse):
    template = 'wls2oauth2/error.html'
    message = 'Web Login System error'

    def __init__(self, *args, **kwargs):
        context = {'message': self.message}
        return super().__init__(self.template, context, *args, **kwargs)

    def redirect(self, login_service, original_request):
        """
        If the fail query parameter is not "yes", we need to redirect. This method will generate
        the appropriate redirect response given a LoginService instance.

        """
        return redirect(login_service.generate_failure(
            self.status_code, original_request, msg=self.message
        ).redirect_url)


class HttpRequestParameterErrorResponse(ErrorResponseBase):
    status_code = ucam_wls.status.REQUEST_PARAM_ERROR
    message = 'There was a problem decoding the request parameters.'


class HttpUnsupportedProtocolResponse(ErrorResponseBase):
    status_code = ucam_wls.status.UNSUPPORTED_PROTO_VER
    message = 'This server does not support the version of the protocol specified.'


class HttpNoMutallyAcceptedAuthTypesResponse(ErrorResponseBase):
    status_code = ucam_wls.status.NO_MUTUAL_AUTH_TYPES
    message = 'No mutually acceptable authentication types available.'


class HttpInteractionRequiredResponse(ErrorResponseBase):
    status_code = ucam_wls.status.INTERACTION_REQUIRED
    message = 'Interactive login is required.'


class AuthenticateView(View):
    def get(self, request, *args, **kwargs):
        # Extract the query string from the request
        query_str = request.GET.urlencode()
        LOG.info('Auth request query: %s', query_str)

        # Parse the WLS request from the query string. Propagate errors back to caller via HTTP
        # status codes. We do not respect the "fail" query parameter since we can't trust its value
        # until the query string has been parsed.
        try:
            auth_request = ucam_wls.AuthRequest.from_query_string(query_str)
        except InvalidAuthRequest:
            LOG.exception('Invalid request')
            return HttpRequestParameterErrorResponse()
        except ProtocolVersionUnsupported:
            LOG.exception('Unsupported protocol')
            return HttpUnsupportedProtocolResponse()
        except Exception:
            LOG.exception('Unknown error when parsing query string')
            return HttpRequestParameterErrorResponse()

        # Get the current login service instance
        login_service = get_login_service()

        # Check authentication type
        if not login_service.have_mutual_auth_type(auth_request):
            # Note that "fail" can be None, False or True
            response = HttpNoMutallyAcceptedAuthTypesResponse()
            LOG.error('No mutual auth types')
            if auth_request.fail is True:
                return response
            else:
                return response.redirect(login_service, auth_request)

        # Compute expiry time
        expiry = datetime.datetime.utcnow() + datetime.timedelta(seconds=30)

        if not request.user.is_authenticated:
            # If the user is not authenticated *and* interactive authentication is not forbidden,
            # authenticate. (Note that auth_request.iact can be True, False or None.)
            if auth_request.iact is not False:
                LOG.info('Redirecting to sign in')
                return redirect_to_login(request)

            # If we got here, interactive authentication is not permitted by the authentication
            # request and so return an appropriate error code. Note that "fail" can be None, False
            # or True
            response = HttpInteractionRequiredResponse()
            LOG.error('Interactive sign in required')
            if auth_request.fail is True:
                return response
            else:
                return response.redirect(login_service, auth_request)

        # If this session was actively authenticated, redirect_to_login() will have set a value in
        # the session state.
        was_actively_authenticated = request.session.pop('wls2oauth2__did_login_redirect', False)

        # If iact is True and there wasn't active authentication, we must re-authenticate. Again,
        # note that iact could be False or None.
        if auth_request.iact is True and not was_actively_authenticated:
            return redirect_to_login(request)

        # Construct the authenticated principal based on the current user
        principal = ucam_wls.AuthPrincipal(
            userid=request.user.username, auth_methods=['pwd'], session_expiry=expiry
        )

        # At the moment, Raven OAuth2 can only authenticate current members of the University and
        # so all successful authentication has the "current" ptag set. If this shim is extended to
        # support "Raven for life" users, we need to re-visit this.
        principal.ptags = ['current']

        # Construct the WLS response.
        if was_actively_authenticated:
            response = login_service.authenticate_active(auth_request, principal, 'pwd')
        else:
            response = login_service.authenticate_passive(auth_request, principal)

        LOG.info('Redirecting with authentication success')
        return redirect(response.redirect_url)


def redirect_to_login(request):
    """
    Return a HttpResponse which redirects to a login page and then back to the original request.

    We set a value in the session state indicating that an active login was requested. This is used
    in the AuthenticateView implementation to distinguish between an "active" and "passive"
    authentication.

    """
    request.session['wls2oauth2__did_login_redirect'] = True

    # Construct a redirect back to the authentication view.
    request_qs = urllib.parse.urlencode(request.GET, doseq=True)
    request_url = f'{request.path}?{request_qs}' if request_qs != '' else request.path
    login_qs = urllib.parse.urlencode({'next': request_url})
    login_url = urllib.parse.urljoin(settings.LOGIN_URL, f'?{login_qs}')
    return redirect(login_url)
